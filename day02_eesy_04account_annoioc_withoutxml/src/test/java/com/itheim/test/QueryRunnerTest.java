package com.itheim.test;

import com.itheima.config.SpringConfiguration;
import org.apache.commons.dbutils.QueryRunner;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

// 测试QueryRunner是否是单例
public class QueryRunnerTest {
    @Test
    public void testQueryRunner() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        // 获取queryRunner对象
        QueryRunner runner1 = applicationContext.getBean("runner", QueryRunner.class);
        QueryRunner runner2 = applicationContext.getBean("runner", QueryRunner.class);
        System.out.println(runner1 == runner2);
    }
}
