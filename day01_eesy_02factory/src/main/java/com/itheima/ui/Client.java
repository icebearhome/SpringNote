package com.itheima.ui;

import com.itheima.day01_eesy_02factory.factory.BeanFactory;
import com.itheima.day01_eesy_02factory.service.AccountService;

public class Client {
    public static void main(String[] args) {
        // 这里导致了较强的耦合
        // AccountService accountService = new AccountServiceImpl();
        // 解耦
        for (int i = 0; i < 5; i++) {
            AccountService accountService = (AccountService) BeanFactory.getBean("accountService");
            System.out.println(accountService);
            accountService.saveAccount();
        }
    }
}
