package com.itheima.factory;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/*
*   创建 Bean 对象的工厂
*       Bean:在计算机英语中有可重用组件的含义
*       JavaBean: 用Java语言编写的可重用组件
*            不仅仅是意味着实体类 JavaBean >> 实体类
*
*   创建 Service 和 Dao 对象的
*   1. 需要一个配置文件配置我们的 service 和 dao
*      配置文件的内容: 唯一标识 = 全限定列明 (key == value)
*   2. 通过读取配置文件中的内容 反射创建对象
*
*   配置文件可以使用 xml 和 properties
*   properties的配置结构简单
* */
public class BeanFactory {

    // 定义一个 properties 对象
    private static Properties props;

    // 定义一个map用来存放我们创建的对象 我们称之为容器
    private static Map<String, Object> beans;

    // 使用静态代码块为Properties对象赋值
    // 静态加载器只在类加载的时候加载一次
    static{
        try {
            // 实例化对象
            props = new Properties();
            // 获取properties文件的流对象
            // 不可以使用这个写法 因为路径没法书写
            //InputStream in = new FileInputStream();
            InputStream in = BeanFactory.class.getClassLoader().getResourceAsStream("bean.properties");
            props.load(in);
            beans = new HashMap<>();
            // 取出配置文件中所有的 key
            Enumeration keys = props.keys();
            // 可以遍历枚举
            while(keys.hasMoreElements()){
                // 取出每个 key
                String key = keys.nextElement().toString();
                // 根据key获取value
                String beanPath = props.getProperty(key);
                // 反射创建对象
                Object value = Class.forName(beanPath).newInstance();
                // 把key 和value 存入容器之中
                beans.put(key ,value);
            }
        } catch (Exception e) {
            throw new ExceptionInInitializerError("初始化properties失败!");
        }
    }

    /**
    * Description: 根据bean的名称获取对象 此时的对象已经是单例的了
    */
    public static Object getBean(String beanName){
        return beans.get(beanName);
    }



    // 不能够写成特定的类型
    /**
    * Description: 根据bean的名称获取bean对象

    public static Object getBean(String beanName){
       Object bean = null;
       String beanPath = props.getProperty(beanName);
        try {
            // newInstance(): 表明每次都会调用默认构造函数创建对象
            bean = Class.forName(beanPath).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;
    }*/
}
