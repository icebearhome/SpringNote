package com.itheima.proxy;

/**
 * 一个生产者
 */
public class Producer implements IProducer{

    /**
     * 产品销售方法
     * @param money 获得的利润
     */
    public void saleProduct(float money){
        System.out.println("销售产品,并获取利益 : "  + money);
    }

    /**
     * 售后服务
     * @param money 获得的利润
     */
    public void afterService(float money){
        System.out.println("售后服务,并获取利益 : " + money);
    }
}

