package com.itheima.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.springframework.context.annotation.*;

import javax.sql.DataSource;

// 这个类是一个配置类 它的作用和 bean.xml 是一样的
/*
* @Configuration:指定当前类是一个配置类
*   当配置类作为 AnnotationConfigApplicationContext 对象创建的参数的时候 该注解可以不写+
* @ComponentScan:用于通过注解指定Spring在创建容器时要扫描的包
*   属性:value: 它和 basePackages 的作用是一样的 都是用于指定创建容器时要扫描的包
*               我们使用此注解就相当于在xml中配置了
*                       <context:component-scan base-package="com.itheima"></context:component-scan>
* @Bean:用于把当前方法的返回值作为Bean对象 存入Spring的IOC容器中
*   属性: name: 用于指定Bean的ID 当不写的时候 默认值是当前方法的名称
*   细节: 当我们使用注解配置方法时候 如果方法有参数 Spring框架会去容器中查找有没有可用的Bean对象
*        查找的方式和 @Autowired注解的作用是一致的
* @Import: 用于导入其他的配置类
*   属性: value: 用于指定其他配置类的字节码
*       当我们使用Import注解字后 有@Import注解的就是父配置类 导入的就是子配置类
* @PropertySource: 用于指定properties文件的位置
*   属性: value: 指定文件的名称和路径
*   关键字: classpath: 表示类路径下
*
* */
//@Configuration
@ComponentScan("com.itheima")
@Import(JDBCConfig.class)
@PropertySource("classpath:jdbcConfig.properties")
public class SpringConfiguration {




}
