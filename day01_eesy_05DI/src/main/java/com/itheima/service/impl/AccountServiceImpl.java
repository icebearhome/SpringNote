package com.itheima.service.impl;

import com.itheima.service.AccountService;

import java.util.Date;

// 业务层的实现类
public class AccountServiceImpl implements AccountService {

    // 如果是经常变化的数据 并不适用于【注入】的方式
    private String name;

    private Integer age;

    private Date birthday;

    public AccountServiceImpl(String name, Integer age, Date birthday){
        this.name = name;
        this.age = age;
        this.birthday = birthday;
    }

    @Override
    public void saveAccount() {
        System.out.println("Service中的saveAccount方法执行了!" + name + age + birthday);
    }



}
