package com.itheima.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.validation.annotation.Validated;

import javax.sql.DataSource;

/*
和Spring连接数据库相关的配置类
    在SpringConfiguration中直接使用 @Import注解标注引用这个配置类文件 所以可以省去@Configuration注解
 */
@Configuration
public class JDBCConfig {
    // 用于创建一个 QueryRunner 对象
    /*    <bean id="runner" class="org.apache.commons.dbutils.QueryRunner" scope="prototype">
            <constructor-arg name="ds" ref="dataSource"></constructor-arg>
          </bean>
          使用这个配置创建之后会直接放入Spring容器之中
    */
    @Value("${jdbc.driver}")
    private String driver;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    @Bean(name = "runner")
    /*
     * 细节上面的问题:这里的QueryRuuner类一定是一个单例的
     * */
    @Scope("prototype")
    public QueryRunner createQueryRunner(@Qualifier("dataSource2") DataSource dataSource){
        return new QueryRunner(dataSource);
    }

    // 创建数据源 对象
    @Bean(name = "dataSource")
    public DataSource createDataSource(){
        try{
            ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
            comboPooledDataSource.setDriverClass(driver);
            comboPooledDataSource.setJdbcUrl(url);
            comboPooledDataSource.setUser(username);
            comboPooledDataSource.setPassword(password);
            return comboPooledDataSource;
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    // 创建数据源 对象
    @Bean(name = "dataSource2")
    public DataSource createDataSource2(){
        try{
            ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
            comboPooledDataSource.setDriverClass(driver);
            comboPooledDataSource.setJdbcUrl("jdbc:mysql://localhost:3306/eesy02?serverTimezone=UTC&useUnicode=true&characterEncoding=utf8");
            comboPooledDataSource.setUser(username);
            comboPooledDataSource.setPassword(password);
            return comboPooledDataSource;
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }
}
