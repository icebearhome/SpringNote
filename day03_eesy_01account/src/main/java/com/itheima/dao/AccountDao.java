package com.itheima.dao;

import com.itheima.entity.Account;

import java.util.List;

// 账户的持久层接口
public interface AccountDao {

    List<Account> findAllAccount();

    Account findAccountById(Integer accountId);

    void saveAccount(Account account);

    void updateAccount(Account account);

    void deleteAccount(Integer accountId);

    /**
     * 根据名称查询账户
     * @param accoutName
     * @return 如果有唯一的值 就返回 如果没有结果就返回null
     *         如果结果集超过一个就抛出异常
     */
    Account findAccountByName(String accoutName);
}
