package com.itheima.service.impl;


import com.itheima.dao.AccountDao;
import com.itheima.dao.impl.AccountDaoImpl;
import com.itheima.service.AccountService;

// 业务层的实现类
public class AccountServiceImpl implements AccountService {

     private AccountDao accountDao = new AccountDaoImpl();

     // 使用构造函数
     public AccountServiceImpl(){
         System.out.println("对象创建了!");
     }

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }
}
