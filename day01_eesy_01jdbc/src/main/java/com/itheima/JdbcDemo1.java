package com.itheima;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
*   Description: 讲解程序的耦合
 *      耦合: 程序间的依赖关系
 *          如果我们的程序丢失了引入的jar包 就会直接调至编译期报错
 *      包括 类之间的依赖 和 方法间的依赖
 *      解耦: 降低程序间的依赖关系
 *
 *      实际开发的时候需要做到: 编译期不依赖 运行时才依赖!
 *
 *      解耦的思路:
 *          第一步: 使用反射来创建对象 避免使用new关键字
 *              存在的问题: 字符串是写死的 更改加载的文件需要修改源代码
 *          第二步: 通过读取配置文件来获取需要创建的对象的全限定类名
*/
public class JdbcDemo1 {
    public static void main(String[] args) throws Exception {
        // 1. 注册驱动
        //DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        // 按照下面这种写法 这里的全限定类名只是表示一段字符串 我们不直接依赖于某个类
        // 由于缺失这个类 一定不会产生编译期报错 会产生运行时异常
        Class.forName("com.mysql.jdbc.Driver");
        // 2. 获取连接
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/eesy?useUnicode=true&characterEncoding=utf8","root","root");
        // 3. 获取数据库的预处理对象
        PreparedStatement pstm = conn.prepareStatement("SELECT * FROM account");
        // 4. 执行SQL 得到结果集
        ResultSet rs = pstm.executeQuery();
        // 5. 遍历结果集
        while(rs.next()){
            System.out.println(rs.getString("name"));
        }
        // 6. 释放资源
        rs.close();
        pstm.close();
        conn.close();
    }
}
