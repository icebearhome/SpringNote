package com.itheima.ui;


import com.itheima.service.AccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {

    public static void main(String[] args) {
        /*ApplicationContext*/
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        /*AccountService accountService1 = (AccountService)applicationContext.getBean("accountService");
        accountService1.saveAccount();*/
        AccountService accountService = (AccountService) applicationContext.getBean("accountService3");
        accountService.saveAccount();

        //applicationContext.close();
    }
}
