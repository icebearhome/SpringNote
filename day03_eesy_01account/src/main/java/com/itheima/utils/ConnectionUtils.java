package com.itheima.utils;

import javax.sql.DataSource;
import java.sql.Connection;
/**
 * 连接的工具类
 *  它用于从数据源中获取一个链接 并实现和线程的绑定
 */
public class ConnectionUtils {
    private ThreadLocal<Connection> t1 = new ThreadLocal<Connection>();

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Connection getThreadConnection(){


        // 1. 先从ThreadLocal上获取
        Connection conn = t1.get();
        try {
            // 2. 判断当前线程上是否有链接
            if (conn == null) {
                conn = dataSource.getConnection();
                t1.set(conn);
            }
            return conn;
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    /**
     * 把链接和线程解绑
     */
    public void removeConnection(){
        t1.remove();
    }
}
