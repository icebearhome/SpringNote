package com.itheima.ui;


import com.itheima.dao.AccountDao;
import com.itheima.service.AccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {

    public static void main(String[] args) {
        // 获取核心容器对象
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        AccountService accountService = (AccountService)applicationContext.getBean("accountService");
        //AccountService accountService2 = (AccountService)applicationContext.getBean("accountService");
        //AccountDao accountDao = applicationContext.getBean("accountDao", AccountDao.class);
        //System.out.println(accountService);
        accountService.saveAccount();
        applicationContext.close();
    }
}
