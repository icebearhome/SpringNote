package com.itheim.test;

import com.itheima.config.SpringConfiguration;
import com.itheima.entity.Account;
import com.itheima.service.AccountService;
import javafx.application.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLOutput;
import java.util.List;

/*
* 使用Junit单元测试 测试我们的配置
* Spring整合Junit配置
*   1. 导入Spring 整合junit的jar包
*   2. 使用 junit 的注解 把原有的main方法替换成Spring提供的main方法 @RunWith
*   3. 告知 Spring 的运行器 Spring和IOC创建是基于xml还是基于注解的 并说明位置
*       @ContextConfiguration:
*              locations: 指定xml文件的位置 加上classpath关键字表明在类路径下
*               classes: 指定注解类所在的位置
* 当使用Spring5版本的时候 要求Junit的jar必须是 4.12以上
* */
@ContextConfiguration(classes = SpringConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountServiceTest {

    /*
    为什么这里的注解失效了呢>
        程序的入口是 main方法
        junit单元测试中 junit集成了一个main方法 该方法就会判断当前测试类中哪些方法有@Test注解
            junit就会让有Test注解的方法执行
        Junit不会在意我们是否使用Spring框架
            在执行测试方法的时候 JUnit根本不知道我们是否使用了Spring框架
            所以也不会为我们读取配置文件/配置类创建Spring核心容器
     综上所述 当测试方法执行的时候 没有IOC容器 就算写了@Autowired注解 也无法实现注入
    * */
    @Autowired
    private AccountService accountService;
//-
//    @Before
//    public void init(){
//        applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
//        accountService = applicationContext.getBean("accountService", AccountService.class);
//    }

    @Test
    public void testFndAll() {
        // 3. 执行方法
        List<Account> accounts = accountService.findAllAccount();
        for(Account account : accounts){
            System.out.println(account);
        }
    }
    @Test
    public void testFindOne() {
        Account account = accountService.findAccountById(1);
        System.out.println(account);
    }
    @Test
    public void testSave() {
        Account account = new Account();
        account.setName("iceBear");
        account.setMoney(100000000f);
        accountService.saveAccount(account);
    }
    @Test
    public void testUpdate() {
        Account account = accountService.findAccountById(4);
        account.setName("change");
        accountService.updateAccount(account);
    }
    @Test
    public void testDelete() {
        accountService.deleteAccount(4);
    }
}
