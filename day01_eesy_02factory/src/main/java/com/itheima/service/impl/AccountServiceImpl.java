package com.itheima.service.impl;


import com.itheima.dao.AccountDao;
import com.itheima.factory.BeanFactory;
import com.itheima.service.AccountService;

// 业务层的实现类
public class AccountServiceImpl implements AccountService {

    // 这里的 new 是不是需要避免? 是不是导致了耦合?
    /*
    *   如果我们把实现类删除 因为耦合现象
    *   会导致 service 会产生编译期间错误
    * */
    /*
    * 两种截然不同的创建对象的方式
    * 1. new 关键字 这个时候是主动的和资源获得联系
    * 2. 使用IOC 控制反转创建对象 能够削减计算机的耦合程度
    * */

    // 使用 new 关键字完全可以自主的找到想要创建的对象 AccountDaoImpl
    // private AccountDao accountDao = new AccountDaoImpl();
    // 这种方式放弃了自主寻找这个类 而是通过工厂来获取 这个类无法独立的自主控制
    private AccountDao accountDao = (AccountDao) BeanFactory.getBean("accountDao");

    // 尽量不要定义成为类成员 而是定义到方法体中
    // 这样可以避免出现单例对象数据不安全的情况发生
    // private int i = 1;

    @Override
    public void saveAccount() {
        int i = 1;
        accountDao.saveAccount();
        System.out.println(i);
        i++;
    }
}
