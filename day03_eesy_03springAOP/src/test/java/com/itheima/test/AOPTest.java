package com.itheima.test;

import com.itheima.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试AOP配置
 */
public class AOPTest {
    public static void main(String[] args) {
        // 1. 获取容器
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        // 2. 获取对象
        IAccountService accountService = (IAccountService) applicationContext.getBean("accountService");
        // 3. 执行方法
        accountService.saveAccount();
        accountService.updateAccount(1);
        accountService.deleteAccount();
    }
}
