package com.itheima.ui;


import com.itheima.dao.AccountDao;
import com.itheima.service.AccountService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


import java.util.function.BiFunction;

public class Client {
    /*
        ApplicationContext的三个常用实现类:
           1. ClassPahtXmlApplicationContext 它可以加载类路径下配置文件 要求配置文件一定在类路径下
           2. FileSystemXmlApplicationContext 它可以加载磁盘任意路径下的配置文件(必须有访问权限)
           3. AnnotationConfigApplicationContext 它是用于读取注解创建容器的
        核心容器的两个接口引发出的问题:
            ApplicationContext  单例对象适用  更多的时候适用这个方法定义对象
                它在构建核心容器时 创建对象采取的策略是采用立即加载的方式
                也就是说 读取完配置文件就马上创建配置文件中的配置对象
            BeanFactory  多例对象适用
                它在创建对象采取的策略是延迟加载的方式
                也就是说什么时候根据ID获取对象了 什么时候才真正的创建对象
    */
    // 获取Spring容器的IOC核心容器并根据ID获取对象
    public static void main(String[] args) {
        // 获取核心容器对象
        //ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        //ApplicationContext applicationContext = new FileSystemXmlApplicationContext("E:\\SSM\\Spring\\day01_eesy_03spring\\src\\main\\resources\\bean.xml");
        // 根据ID获取Bean对象
        //AccountService accountService = (AccountService)applicationContext.getBean("accountService");
        //AccountDao accountDao = applicationContext.getBean("accountDao", AccountDao.class);
        //System.out.println(accountService);
        //System.out.println(accountDao);
        //accountService.saveAccount();
        //--------------------------------------------------------------------------------------------------
        //Resource resource = new ClassPathResource("bean.xml");
        //BeanFactory beanFactory = new XmlBeanFactory(resource);
        //AccountService accountService = (AccountService)beanFactory.getBean("accountService");
        //System.out.println(accountService);
    }
}
