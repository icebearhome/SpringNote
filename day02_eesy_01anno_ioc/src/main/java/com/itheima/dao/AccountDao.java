package com.itheima.dao;

// 账户的持久层接口
public interface AccountDao {
    /**
    * Description: 模拟保存账户的操作
    */
    void saveAccount();
}
