package com.itheima.service.impl;

import com.itheima.service.AccountService;

// 业务层的实现类
public class AccountServiceImpl implements AccountService {

     // 使用构造函数
     public AccountServiceImpl(){
         System.out.println("对象创建了!");
     }

    @Override
    public void saveAccount() {
        System.out.println("Service中的saveAccount方法执行了!");
    }

    public void init(){
        System.out.println("对象初始化方法!");
    }

    public void destroy(){
        System.out.println("-----------------------------");
         System.out.println("对象销毁了!");
    }

}
