package com.itheima.ui;


import com.itheima.service.AccountService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import sun.misc.ASCIICaseInsensitiveComparator;


import java.util.function.BiFunction;

public class Client {

    public static void main(String[] args) {
        // 获取核心容器对象
        // 这里是多态的写法 不能调用子类中的close()方法
        /*ApplicationContext*/
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        // 根据ID获取Bean对象
        AccountService accountService1 = (AccountService)applicationContext.getBean("accountService");
        accountService1.saveAccount();
        // 手动关闭容器
        applicationContext.close();

    }
}
