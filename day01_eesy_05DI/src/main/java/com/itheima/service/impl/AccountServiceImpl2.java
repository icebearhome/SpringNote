package com.itheima.service.impl;

import com.itheima.service.AccountService;

import java.util.Date;

// 业务层的实现类
public class AccountServiceImpl2 implements AccountService {

    // 如果是经常变化的数据 并不适用于【注入】的方式
    private String name;

    private Integer age;

    private Date birthday;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public void saveAccount() {
        System.out.println("Service中的saveAccount方法执行了!" + name + age + birthday);
    }


}
