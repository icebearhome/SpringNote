package com.itheim.test;

import com.itheima.entity.Account;
import com.itheima.service.AccountService;
import javafx.application.Application;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLOutput;
import java.util.List;

/*
* 使用Junit单元测试 测试我们的配置
* */
public class AccountServiceTest {
    @Test
    public void testFndAll() {
        // 1. 获取容器
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        // 2. 得到业务层对象
        AccountService accountService = applicationContext.getBean("accountService", AccountService.class);
        // 3. 执行方法
        List<Account> accounts = accountService.findAllAccount();
        for(Account account : accounts){
            System.out.println(account);
        }
    }
    @Test
    public void testFindOne() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        AccountService accountService = applicationContext.getBean("accountService", AccountService.class);
        Account account = accountService.findAccountById(1);
        System.out.println(account);
    }
    @Test
    public void testSave() {
        Account account = new Account();
        account.setName("iceBear");
        account.setMoney(100000000f);
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        AccountService accountService = applicationContext.getBean("accountService", AccountService.class);
        accountService.saveAccount(account);
    }
    @Test
    public void testUpdate() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        AccountService accountService = applicationContext.getBean("accountService", AccountService.class);
        Account account = accountService.findAccountById(4);
        account.setName("change");
        accountService.updateAccount(account);
    }
    @Test
    public void testDelete() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        AccountService accountService = applicationContext.getBean("accountService", AccountService.class);
        accountService.deleteAccount(4);
    }
}
