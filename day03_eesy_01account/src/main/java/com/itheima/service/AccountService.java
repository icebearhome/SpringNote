package com.itheima.service;

import com.itheima.entity.Account;

import java.util.List;

/*
* 账户的业务层接口
* */
public interface AccountService {

    List<Account> findAllAccount();

    Account findAccountById(Integer accountId);

    void saveAccount(Account account);

    void updateAccount(Account account);

    void deleteAccount(Integer accountId);

    void transfer(String sourceName, String targetName, Float money);
}
