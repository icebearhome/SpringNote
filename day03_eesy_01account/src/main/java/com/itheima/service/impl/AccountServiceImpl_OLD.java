package com.itheima.service.impl;

import com.itheima.dao.AccountDao;
import com.itheima.entity.Account;
import com.itheima.service.AccountService;
import com.itheima.utils.TransactionManager;

import java.util.List;

/**
 * 账户业务的实现类
 * 事务控制应该是在业务层存的
 */
public class AccountServiceImpl_OLD implements AccountService {

    private AccountDao accountDao;

    private TransactionManager transactionManager;
    public void setTransactionManager(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public List<Account> findAllAccount() {
        try {
            // 开启事务
            transactionManager.beginTransaction();
            // 执行操作
            List<Account> allAccount = accountDao.findAllAccount();
            // 提交事务
            transactionManager.commit();
            // 返回结果
            return allAccount;
        } catch (Exception e) {
            // 回滚操作
            transactionManager.rollback();
            throw new RuntimeException(e);
        }finally {
            // 释放连接
            transactionManager.release();
        }
    }

    @Override
    public Account findAccountById(Integer accountId) {
        try {
            // 开启事务
            transactionManager.beginTransaction();
            // 执行操作
            Account account = accountDao.findAccountById(accountId);
            // 提交事务
            transactionManager.commit();
            // 返回结果
            return account;
        } catch (Exception e) {
            // 回滚操作
            transactionManager.rollback();
            throw new RuntimeException(e);
        }finally {
            // 释放连接
            transactionManager.release();
        }
    }

    @Override
    public void saveAccount(Account account) {
        try {
            // 开启事务
            transactionManager.beginTransaction();
            // 执行操作
            accountDao.saveAccount(account);
            // 提交事务
            transactionManager.commit();
            // 返回结果
        } catch (Exception e) {
            // 回滚操作
            transactionManager.rollback();
        }finally {
            // 释放连接
            transactionManager.release();
        }
    }

    @Override
    public void updateAccount(Account account) {
        try {
            // 开启事务
            transactionManager.beginTransaction();
            // 执行操作
            accountDao.updateAccount(account);
            // 提交事务
            transactionManager.commit();
            // 返回结果
        } catch (Exception e) {
            // 回滚操作
            transactionManager.rollback();
        }finally {
            // 释放连接
            transactionManager.release();
        }
    }

    @Override
    public void deleteAccount(Integer accountId) {
        try {
            // 开启事务
            transactionManager.beginTransaction();
            // 执行操作
            accountDao.deleteAccount(accountId);
            // 提交事务
            transactionManager.commit();
            // 返回结果
        } catch (Exception e) {
            // 回滚操作
            transactionManager.rollback();
        }finally {
            // 释放连接
            transactionManager.release();
        }
    }

    @Override
    public void transfer(String sourceName, String targetName, Float money) {
        /*
            总共和数据库交互了四次 一共获取四次连接 这说明一共有4个事务
            我们需要统一事务 所以需要使用一个 connection 连接来控制
            所以我们需要使用 ThreadLocal 对象将 Connection 和当前线程绑定
            从而使线程中只有一个能控制事务的对象
         */
        try {
            // 1.开启事务
            transactionManager.beginTransaction();
            // 2.执行操作
            // 2.1 根据名称查询转出账户
            Account source = accountDao.findAccountByName(sourceName);
            // 2.2 根据名称查询转入账户
            Account target = accountDao.findAccountByName(targetName);
            // 2.3 转出账户金额减少
            source.setMoney(source.getMoney() - money);
            // 2.4 转出账户金额增加
            target.setMoney(target.getMoney() + money);
            // 2.5 更新转出账户
            accountDao.updateAccount(source);
            // 模拟出现故障
            /**
             * @Description: 出现了大问题! 因为代码的错误导致出现了数据不一致的问题!
             * 转入账户金额数据更新失败!
             */
            int i = 1 / 0;

            // 2.6 更新转入账户
            accountDao.updateAccount(target);
            // 3. 提交事务
            transactionManager.commit();
        } catch (Exception e) {
            // 4. 回滚操作
            transactionManager.rollback();
            e.printStackTrace();
        }finally {
            // 5. 释放连接
            transactionManager.release();
        }
    }

}
