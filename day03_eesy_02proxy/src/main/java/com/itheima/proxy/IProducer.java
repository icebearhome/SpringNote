package com.itheima.proxy;

/**
 * 对生产厂家要求的接口
 */
public interface IProducer {
    /**
     * 产品销售方法
     * @param money 获得的利润
     */
    public void saleProduct(float money);

    /**
     * 售后服务
     * @param money 获得的利润
     */
    public void afterService(float money);
}
