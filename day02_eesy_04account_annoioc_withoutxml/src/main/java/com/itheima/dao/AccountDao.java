package com.itheima.dao;

import com.itheima.entity.Account;

import java.util.List;

// 账户的持久层接口
public interface AccountDao {

    List<Account> findAllAccount();

    Account findAccountById(Integer accountId);

    void saveAccount(Account account);

    void updateAccount(Account account);

    void deleteAccount(Integer accountId);
}
