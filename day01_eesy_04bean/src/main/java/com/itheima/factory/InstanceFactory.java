package com.itheima.factory;

import com.itheima.service.AccountService;
import com.itheima.service.impl.AccountServiceImpl;

/*
* 模拟一个工厂类 这个类可能是存在于jar包当中的
* 我们无法通过修改源码的方式修改类提供默认构造函数
* */
public class InstanceFactory {
    public AccountService getAccountSerivice(){
        return new AccountServiceImpl();
    }
}
