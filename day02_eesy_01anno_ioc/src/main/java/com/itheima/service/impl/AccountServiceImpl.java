package com.itheima.service.impl;


import com.itheima.dao.AccountDao;
import com.itheima.dao.impl.AccountDaoImpl;
import com.itheima.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

// 业务层的实现类
/*
    <bean id="accountService" class="com.itheima.service.impl.AccountServiceImpl" scope="singleton"
    init-method="init" destroy-method="destroy">
        <property name="" value="" / ref=""> </property>
    </bean>


    用于创建对象的注解:
        它们的作用就和在xml配置文件中编写一个<bean>标签实现的功能是一致的
            @Component: 用于把当前类存入Spring容器中 Spring容器是一个Map结构 存储对象 对象是Value 那么Key值是什么呢?
                属性:value - 用于指定bean的id 当我们不写的时候 默认是当前类名且首字母改小写

            @Controller: 一般用在【表现层】
            @Service： 一般用在【业务层】
            @Repository： 一般用在【持久层】
            以上三个注解和 @Component 是一样的 他们三个是Spring框架为我们提供明确的三层使用的注解 使我们的三层对象更加清晰

    用于注入数据的注解
            @Autowared: 自动按照类型注入 只要容器中有【唯一的】一个bean对象类型和要注入的变量类型匹配就可以注入成功
                        如果IOC容器中 没有任何bean的类型和要注入的变量类型匹配则报错
                        如果IOC容器中有多个类型匹配时 会使用变量名称作为 bean 的id 继续查找 看看手否有匹配
                            如果没有Key能够匹配 则会报错
                常用位置: 成员变量/方法
                细节: 使用注解注入的时候 set方法就不是必须的了
            @Qualifier: 在按照类型注入的基础之上 再按照名称进行注入 它在给成员注入的时候不能单独使用 必须和@Autowared配合使用
                        在给方法参数注入的时候可以单独使用
                属性: value 用于指定用于注入的 id
            @Resource: 直接按照bean的id进行注入 它可以单独使用
                属性: name: 用于指定bean的id
            以上三个注解都只能注入其他bean类型的数据 而基本类型和String类型无法通过上述注解来实现
            另外, 集合类型的注入只能通过xml来实现
        =========================================================
        基本类型和String类型如何注入呢?
            @Value: 用于注入基本类型和String类型的数据
                    属性:value 用于指定数据的值 可以使用Spring中的SpEL
                        SpEL的写法: ${表达式}

        它们的作用就和在xml配置文件中编写一个<property>标签的作用是一样的
    用于改变作用范围的注解
        它们的作用就和在bean标签中使用scope属性实现的功能是一样的
        @Scope: 用于指定bean的作用范围
            属性: value 指定范围的取值 常用取值: singleton / prototype 默认是单例的
    和生命周期相关的注解 了解即可
        它们的作用就和在bean标签中使用 init-method 和 destory-method 的作用是一样的
        @PreDestory: 用于指定销毁方法
        @PostConstruct: 用于指定初始化方法
 * */
// 如果只有一个属性值 那么 value 可以暂时省略不写 如果有两个属性值 则 value 必须得写
@Service(value="accountService")
//@Scope("prototype")   多例对象交给 Java 的垃圾回收机制处理
public class AccountServiceImpl implements AccountService {

     //@Autowired // 自动按照类型就注入了
     //@Qualifier("accountDao1")
     @Resource(name = "accountDao1")
     // 这里的 AccountDao 是没有使用 new 关键字实例化的
     private AccountDao accountDao;
     // 使用构造函数
     public AccountServiceImpl(){
         System.out.println("对象创建了!");
     }

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }

    @PostConstruct
    public void init() {
        System.out.println("初始化方法执行了!");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("销毁方法执行了");
    }
}
